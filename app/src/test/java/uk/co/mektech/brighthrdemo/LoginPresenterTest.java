package uk.co.mektech.brighthrdemo;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import uk.co.mektech.brighthrdemo.feature.login.LoginDataInteractor;
import uk.co.mektech.brighthrdemo.feature.login.LoginPresenter;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class LoginPresenterTest {

    private static final String TEST_VALID_EMAIL_ADDRESS = "valid@email.com";
    private static final String TEST_VALID_PASSWORD = "password";

    private static final String TEST_TIMEZONE = "Europe/London";

    @Mock
    private LoginPresenter.LoginView view;

    @Mock
    private LoginDataInteractor dataInteractor;

    private LoginPresenter presenter;

    @Before
    public void setup() {
        presenter = new LoginPresenter(view, dataInteractor);
    }

    @Test
    public void on_view_ready_should_disable_button() throws Exception {
        presenter.onViewReady();

        verify(view).disableButton();
    }

    @Test
    public void on_text_changed_valid_email_no_password_should_disable_button() throws Exception {
        when(view.getInputEmail()).thenReturn(TEST_VALID_EMAIL_ADDRESS);
        when(view.getInputPassword()).thenReturn("");

        presenter.onInputTextChanged();

        verify(view).disableButton();
    }

    @Test
    public void on_text_changed_valid_email_invalid_password_should_disable_button() throws Exception {
        when(view.getInputEmail()).thenReturn(TEST_VALID_EMAIL_ADDRESS);
        when(view.getInputPassword()).thenReturn("   ");

        presenter.onInputTextChanged();

        verify(view).disableButton();
    }

    @Test
    public void on_text_changed_no_email_valid_password_should_disable_button() throws Exception {
        when(view.getInputEmail()).thenReturn("");
        when(view.getInputPassword()).thenReturn(TEST_VALID_PASSWORD);

        presenter.onInputTextChanged();

        verify(view).disableButton();
    }

    @Test
    public void on_text_changed_invalid_email_valid_password_should_disable_button() throws Exception {
        when(view.getInputEmail()).thenReturn("   ");
        when(view.getInputPassword()).thenReturn(TEST_VALID_PASSWORD);

        presenter.onInputTextChanged();

        verify(view).disableButton();
    }

    @Test
    public void on_text_changed_valid_email_valid_password_should_enable_button() throws Exception {
        when(view.getInputEmail()).thenReturn(TEST_VALID_EMAIL_ADDRESS);
        when(view.getInputPassword()).thenReturn(TEST_VALID_PASSWORD);

        presenter.onInputTextChanged();

        verify(view).enableButton();
    }

    @Test
    public void on_login_successful_should_show_timezone() throws Exception {
        presenter.onLoginSuccessful(TEST_TIMEZONE);

        verify(view).hideLoading();
        verify(view).showUserTimezone(TEST_TIMEZONE);
    }

    @Test
    public void on_invalid_user_login_should_show_invalid_login_error() throws Exception {
        presenter.onInvalidUserLogin();

        verify(view).hideLoading();
        verify(view).showInvalidLoginError();
    }

    @Test
    public void on_login_failure_should_show_default_error() throws Exception {
        presenter.onLoginFailure();

        verify(view).hideLoading();
        verify(view).showDefaultError();
    }

    @Test
    public void on_login_pressed_should_perform_login_request() throws Exception {
        presenter.onLoginPressed();

        verify(view).hideKeyboard();
        verify(view).showLoading();
        verify(view).getInputEmail();
        verify(view).getInputPassword();
        verify(dataInteractor).loginUser(anyString(), anyString());
    }

    @Test
    public void on_login_pressed_multiple_times_should_perform_one_request() throws Exception {
        presenter.onLoginPressed();
        presenter.onLoginPressed();
        presenter.onLoginPressed();

        verify(view, times(1)).hideKeyboard();
        verify(view, times(1)).showLoading();
        verify(view, times(1)).getInputEmail();
        verify(view, times(1)).getInputPassword();
        verify(dataInteractor, times(1)).loginUser(anyString(), anyString());
    }
}
