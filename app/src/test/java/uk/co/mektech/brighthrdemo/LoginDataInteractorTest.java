package uk.co.mektech.brighthrdemo;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import io.reactivex.Single;
import retrofit2.Response;
import uk.co.mektech.brighthrdemo.feature.login.LoginDataInteractor;
import uk.co.mektech.brighthrdemo.feature.login.LoginPresenter;
import uk.co.mektech.brighthrdemo.service.UserService;
import uk.co.mektech.brighthrdemo.service.dto.UserDataResponse;


@RunWith(MockitoJUnitRunner.class)
public class LoginDataInteractorTest {

    private static final String TEST_USERNAME = "username";
    private static final String TEST_PASSWORD = "password";

    private LoginDataInteractor dataInteractor = new LoginDataInteractor();

    @Mock
    private LoginPresenter presenter;

    @Mock
    private UserService service;

    @Before
    public void setup() {
        dataInteractor.setPresenter(presenter);
        dataInteractor.setService(service);
    }

    @Test
    public void on_login_successful_presenter_successful_login_called() throws Exception {
        Single<Result<UserDataResponse>> resultSingle = Single.just(Result.response(Response.success(Mockito.mock(UserDataResponse.class))));
        Mockito.when(service.postValidateUser(Mockito.anyString(), Mockito.anyString())).thenReturn(resultSingle);

        dataInteractor.loginUser(TEST_USERNAME, TEST_PASSWORD);

        Mockito.verify(service).buildService(UserService.Server.UK);
        Mockito.verify(service).postValidateUser(TEST_USERNAME, TEST_PASSWORD);
        Mockito.verify(presenter).onLoginSuccessful(Mockito.anyString());
    }
}
