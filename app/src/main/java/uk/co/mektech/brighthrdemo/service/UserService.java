package uk.co.mektech.brighthrdemo.service;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import io.reactivex.Single;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.mektech.brighthrdemo.service.dto.UserDataResponse;
import uk.co.mektech.brighthrdemo.service.dto.ValidateUserRequestBody;

public class UserService {

    Retrofit retrofit;

    public void buildService(Server server) {
        retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(server.getUrl())
                .build();
    }

    public Single<Result<UserDataResponse>> postValidateUser(String username, String password) {
        UserNetworkTask networkTask = retrofit.create(UserNetworkTask.class);
        return networkTask.validateUser(new ValidateUserRequestBody(username, password));
    }

    public enum Server {

        UK("https://brighthr-api-uat.azurewebsites.net/api/Account/"),
        CANADA("https://uat-brighthrapi-canada.azurewebsites.net/api/Account/");

        private String url;

        Server(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

}
