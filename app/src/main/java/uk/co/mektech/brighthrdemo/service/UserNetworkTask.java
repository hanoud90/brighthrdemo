package uk.co.mektech.brighthrdemo.service;

import com.jakewharton.retrofit2.adapter.rxjava2.Result;

import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.POST;
import uk.co.mektech.brighthrdemo.service.dto.UserDataResponse;
import uk.co.mektech.brighthrdemo.service.dto.ValidateUserRequestBody;

public interface UserNetworkTask {

    @POST("PostValidateUser")
    Single<Result<UserDataResponse>> validateUser(@Body ValidateUserRequestBody body);

}
