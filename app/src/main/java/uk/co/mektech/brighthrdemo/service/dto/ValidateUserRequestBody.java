package uk.co.mektech.brighthrdemo.service.dto;

public class ValidateUserRequestBody {

    private String username;
    private String password;

    public ValidateUserRequestBody(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
