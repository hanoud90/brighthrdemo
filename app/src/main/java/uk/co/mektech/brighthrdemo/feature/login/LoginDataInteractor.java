package uk.co.mektech.brighthrdemo.feature.login;


import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Response;
import uk.co.mektech.brighthrdemo.service.UserService;
import uk.co.mektech.brighthrdemo.service.dto.UserDataResponse;

public class LoginDataInteractor {

    private LoginPresenter presenter;
    private UserService service;

    public void setPresenter(LoginPresenter presenter) {
        this.presenter = presenter;
    }

    public void setService(UserService service) {
        this.service = service;
    }

    public void loginUser(String username, String password) {
        service.buildService(UserService.Server.UK);
        service.postValidateUser(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.isError()) {
                        presenter.onLoginFailure();
                        return;
                    }

                    Response<UserDataResponse> response = result.response();
                    if (response.isSuccessful()) {
                        presenter.onLoginSuccessful(response.body().getCompanyTimezone());
                        return;
                    }

                    if (response.code() == 403) {
                        retryWithCanadaServer(username, password);
                        return;
                    }

                    presenter.onLoginFailure();
                });
    }

    private void retryWithCanadaServer(String username, String password) {
        service.buildService(UserService.Server.CANADA);
        service.postValidateUser(username, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(result -> {
                    if (result.isError()) {
                        presenter.onLoginFailure();
                        return;
                    }

                    Response<UserDataResponse> response = result.response();
                    if (response.isSuccessful()) {
                        presenter.onLoginSuccessful(response.body().getCompanyTimezone());
                        return;
                    }

                    if (response.code() == 403) {
                        presenter.onInvalidUserLogin();
                        return;
                    }

                    presenter.onLoginFailure();
                });
    }
}
