package uk.co.mektech.brighthrdemo.ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import uk.co.mektech.brighthrdemo.R;

public class ButtonRounded extends RelativeLayout {

    @BindView(R.id.root)
    ViewGroup root;

    @BindView(R.id.text_view)
    TextView textView;

    @BindView(R.id.progress_bar)
    ProgressBar progressBar;

    public ButtonRounded(Context context, AttributeSet attrs) {
        super(context, attrs);

        View view = LayoutInflater.from(context).inflate(R.layout.component_rounded_button, this);
        ButterKnife.bind(this, view);

        int[] attrsArray = new int[]{
                android.R.attr.text, // 0
        };
        TypedArray attributes = context.obtainStyledAttributes(attrs, attrsArray);
        CharSequence text = attributes.getText(0);
        attributes.recycle();

        textView.setText(text);
    }

    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        root.setOnClickListener(l);
    }

    public void setText(String text) {
        textView.setText(text);
    }

    public void showLoadingSpinner() {
        progressBar.setVisibility(VISIBLE);
        textView.setVisibility(INVISIBLE);
    }

    public void hideLoadingSpinner() {
        progressBar.setVisibility(INVISIBLE);
        textView.setVisibility(VISIBLE);
    }

    public void disable() {
        root.setEnabled(false);
        ViewCompat.setElevation(root, 0);
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.disabled_white));
    }

    public void enable() {
        float scale = getContext().getResources().getDisplayMetrics().density;
        int elevation = (int) (2 * scale + 0.5f);

        root.setEnabled(true);
        ViewCompat.setElevation(root, elevation);
        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.white));
    }

}
