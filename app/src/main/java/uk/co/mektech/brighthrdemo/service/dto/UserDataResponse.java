package uk.co.mektech.brighthrdemo.service.dto;

import com.google.gson.annotations.SerializedName;

/**
 * Created by moh on 08/01/2018.
 */

public class UserDataResponse {

    @SerializedName("companyTimeZoneName")
    private String companyTimezone;

    public String getCompanyTimezone() {
        return companyTimezone;
    }
}
