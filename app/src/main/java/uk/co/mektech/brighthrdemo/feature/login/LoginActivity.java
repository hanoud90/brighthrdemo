package uk.co.mektech.brighthrdemo.feature.login;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import uk.co.mektech.brighthrdemo.R;
import uk.co.mektech.brighthrdemo.service.UserService;
import uk.co.mektech.brighthrdemo.ui.ButtonRounded;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.LoginView, TextWatcher {

    private LoginPresenter presenter;

    @BindView(R.id.edit_text_email)
    AppCompatEditText editTextEmail;

    @BindView(R.id.edit_text_password)
    AppCompatEditText editTextPassword;

    @BindView(R.id.button_login)
    ButtonRounded buttonLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialiseViews();

        initialisePresenter();
    }

    @Override
    public String getInputEmail() {
        return editTextEmail.getText().toString();
    }

    @Override
    public String getInputPassword() {
        return editTextPassword.getText().toString();
    }

    @Override
    public void disableButton() {
        buttonLogin.disable();

    }

    @Override
    public void enableButton() {
        buttonLogin.enable();
    }

    @Override
    public void showUserTimezone(String timezone) {
        Toast.makeText(this, getString(R.string.loginscreen_toast_welcome, timezone), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showInvalidLoginError() {
        Toast.makeText(this, getString(R.string.loginscreen_toast_error_invalid_login), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showDefaultError() {
        Toast.makeText(this, getString(R.string.global_toast_error_generic), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if(imm == null || getCurrentFocus() == null) {
            return;
        }

        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
    }

    @Override
    public void showLoading() {
        buttonLogin.showLoadingSpinner();
    }

    @Override
    public void hideLoading() {
        buttonLogin.hideLoadingSpinner();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
        presenter.onInputTextChanged();
    }

    @OnClick(R.id.button_login)
    public void onLoginPressed() {
        presenter.onLoginPressed();
    }

    private void initialiseViews() {
        ButterKnife.bind(this);
        editTextEmail.addTextChangedListener(this);
        editTextPassword.addTextChangedListener(this);
    }

    private void initialisePresenter() {
        LoginDataInteractor dataInteractor = new LoginDataInteractor();
        dataInteractor.setService(new UserService());

        presenter = new LoginPresenter(this, dataInteractor);
        presenter.onViewReady();
    }
}
