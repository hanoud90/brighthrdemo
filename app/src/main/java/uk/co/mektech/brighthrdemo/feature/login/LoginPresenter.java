package uk.co.mektech.brighthrdemo.feature.login;


public class LoginPresenter {

    private final LoginView view;
    private final LoginDataInteractor dataInteractor;

    private boolean requestInProgress = false;

    public LoginPresenter(LoginView view, LoginDataInteractor dataInteractor) {
        this.view = view;
        this.dataInteractor = dataInteractor;
        this.dataInteractor.setPresenter(this);
    }

    public void onViewReady() {
        view.disableButton();
    }

    public void onInputTextChanged() {
        String email = view.getInputEmail().trim();
        String password = view.getInputPassword().trim();

        if (textIsEmpty(email) || textIsEmpty(password)) {
            view.disableButton();
            return;
        }

        view.enableButton();
    }

    public void onLoginPressed() {
        if (requestInProgress) {
            return;
        }

        requestInProgress = true;
        view.hideKeyboard();
        view.showLoading();
        dataInteractor.loginUser(view.getInputEmail(), view.getInputPassword());
    }

    public void onLoginSuccessful(String timezone) {
        requestInProgress = false;
        view.hideLoading();
        view.showUserTimezone(timezone);
    }

    public void onInvalidUserLogin() {
        requestInProgress = false;
        view.hideLoading();
        view.showInvalidLoginError();
    }

    public void onLoginFailure() {
        requestInProgress = false;
        view.hideLoading();
        view.showDefaultError();
    }

    private boolean textIsEmpty(String text) {
        return text == null || text.isEmpty();
    }


    public interface LoginView {
        String getInputEmail();

        String getInputPassword();

        void disableButton();

        void enableButton();

        void showUserTimezone(String timezone);

        void showInvalidLoginError();

        void showDefaultError();

        void hideKeyboard();

        void showLoading();

        void hideLoading();
    }

}
